#include <Joystick.h>

// Create the Joystick
Joystick_ Joystick(
  JOYSTICK_DEFAULT_REPORT_ID,
  JOYSTICK_TYPE_MULTI_AXIS, 5, 0, 
  false, false, false, 
  false, false, false,
  false, 
  false,
  true,
  true, 
  true
);

// Constant that maps the physical pin to the joystick button.
const int pinToButtonMap = 18;
const int blinkLed = 10;
const int steerPin = A0;
const int acceleratorPin = A1;
const int breakPin = A2;
const int leftButtonPin = 2;
const int leftButtonIndex = 0;
const int rightButtonTopPin = 3;
const int rightButtonTopIndex = 1;
const int rightButtonBottomPin = 4;
const int rightButtonBottomIndex = 2;
const int shiftUpPin = 5;
const int shiftUpIndex = 3;
const int shiftDownPin = 7;
const int shiftDownIndex = 4;

int steerMin = 10;
int steerMax = 1010;
int acceleratorMin = 15;
int acceleratorMax = 1020;
int breakMin = 15;
int breakMax = 715;

int warptime = millis();

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

  pinMode(blinkLed, OUTPUT);
  pinMode(steerPin, INPUT);
  pinMode(acceleratorPin, INPUT_PULLUP);
  pinMode(breakPin, INPUT_PULLUP);
  pinMode(leftButtonPin, INPUT_PULLUP);
  pinMode(rightButtonTopPin, INPUT_PULLUP);
  pinMode(rightButtonBottomPin, INPUT_PULLUP);
  pinMode(shiftUpPin, INPUT_PULLUP);
  pinMode(shiftDownPin, INPUT_PULLUP);

  // Initialize Joystick Library
  Joystick.setSteeringRange(steerMin, steerMax);
  Joystick.setAcceleratorRange(acceleratorMin, acceleratorMax);
  Joystick.setBrakeRange(breakMin, breakMax);
  Joystick.begin();
}

void loop() {
  // put your main code here, to run repeatedly:

  int steerValue = analogRead(steerPin);
  steerValue = map(steerValue, steerMin, steerMax, 0, 1023);
  steerValue = constrain(steerValue, 0, 1023);

  int acceleratorValue = analogRead(acceleratorPin);
  acceleratorValue = map(acceleratorValue, acceleratorMin, acceleratorMax, 0, 1023);
  acceleratorValue = constrain(acceleratorValue, 0, 1023);

  int breakValue = analogRead(breakPin);
  breakValue = map(breakValue, breakMin, breakMax, 0, 1023);
  breakValue = constrain(breakValue, 0, 1023);
  
  int leftButtonState = digitalRead(leftButtonPin);
  int rightButtonTopState = digitalRead(rightButtonTopPin);
  int rightButtonBottomState = digitalRead(rightButtonBottomPin);

  int shiftUpState = digitalRead(shiftUpPin);
  int shiftDownState = digitalRead(shiftDownPin);

  Joystick.setButton(leftButtonIndex, !leftButtonState);
  Joystick.setButton(rightButtonTopIndex, !rightButtonTopState);
  Joystick.setButton(rightButtonBottomIndex, !rightButtonBottomState);
  Joystick.setButton(shiftUpIndex, !shiftUpState);
  Joystick.setButton(shiftDownIndex, !shiftDownState);
  Joystick.setSteering(steerValue);
  Joystick.setAccelerator(acceleratorValue);
  Joystick.setBrake(breakValue);
}

void startCalibration() {
  digitalWrite(blinkLed, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);
  
  int steerValue = analogRead(steerPin);
  // record the maximum sensor value
  if (steerValue > steerMax) {
    steerMax = steerValue;
  }
  // record the minimum sensor value
  if (steerValue < steerMin) {
    steerMin = steerValue;
  }

  int acceleratorValue = analogRead(acceleratorPin);
  // record the maximum sensor value
  if (acceleratorValue > acceleratorMax) {
    acceleratorMax = acceleratorValue;
  }
  // record the minimum sensor value
  if (acceleratorValue < acceleratorMin) {
    acceleratorMin = acceleratorValue;
  }

  int breakValue = analogRead(breakPin);
  // record the maximum sensor value
  if (breakValue > breakMax) {
    breakMax = breakValue;
  }
  // record the minimum sensor value
  if (breakValue < breakMin) {
    breakMin = breakValue;
  }

  Joystick.setSteeringRange(steerMin, steerMax);
  Joystick.setAcceleratorRange(acceleratorMin, acceleratorMax);
  Joystick.setBrakeRange(breakMin, breakMax);

  digitalWrite(blinkLed, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(100);
}
